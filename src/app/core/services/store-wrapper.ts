import store from 'store';

import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StoreWrapperService {
  set(key: string, value: string) {
    store.set(key, value);
  }

  get<T>(key: string): T {
    return store.get(key);
  }

  remove(key: string) {
    store.remove(key);
  }

  clearAll() {
    store.clearAll();
  }
}
