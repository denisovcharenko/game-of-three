import { Component } from '@angular/core';

import { NavbarStore } from '../../stores/navbar.store';
import { GameService } from '../../services/game.service';

@Component({
  selector: 'got-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  constructor(
    public navbarStore: NavbarStore,
    private gameService: GameService
  ) {}

  startNewGame() {
    this.gameService.resetGame();
  }
}
