import { Injectable } from '@angular/core';

import { StoreWrapperService } from '../../core/services/store-wrapper';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  constructor(private storeWrapperService: StoreWrapperService) {}

  get userName(): string {
    return this.storeWrapperService.get<string>('userName');
  }

  set userName(userName: string) {
    this.storeWrapperService.set('userName', userName);
  }
}
