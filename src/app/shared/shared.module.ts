import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NavbarComponent } from './components/navbar/navbar.component';
import { PlayerComponent } from './components/player/player.component';
import { WinnerModalComponent } from './components/winner-modal/winner-modal.component';

const MODULES = [CommonModule];

const MODALS = [WinnerModalComponent];

const COMPONENTS = [NavbarComponent, PlayerComponent];

@NgModule({
  declarations: [...COMPONENTS, ...MODALS],
  imports: [...MODULES],
  exports: [...COMPONENTS, ...MODULES],
  entryComponents: [...MODALS],
  providers: []
})
export class SharedModule {}
